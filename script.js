window.addEventListener('DOMContentLoaded', init)

function init() {
  handleSearch()
  handleSearchShows()
  registerOfflineInfoEvents()
}

function handleSearch() {
  // handle form submission
  document.getElementById('form').addEventListener('submit', formSubmitEvent => {
    formSubmitEvent.preventDefault()
    const showName = formSubmitEvent.target.showName.value

    if (!showName) return

    fetchAndRender(showName)
  })
}

function handleSearchShows() {
  // load previously search shows
  renderSearchShowsList()

  // handle previously searched show click
  const searchedShowsList = document.getElementById('searched-shows-list')
  searchedShowsList.addEventListener('click', e => {
    e.preventDefault()
    const showName = e.target.innerText
    fetchAndRender(showName)
  })

  // clear shows
  document.getElementById('clear-button').addEventListener('click', () => {
    localStorage.setItem('success', '[]')
    searchedShowsList.innerHTML = ''
  })
}

function registerOfflineInfoEvents() {
  const offlineInfoEl = document.getElementById('offline-info')
  if (navigator.onLine) {
    offlineInfoEl.style.display = 'none'
  }

  window.addEventListener('offline', function(e) {
    offlineInfoEl.style.display = 'block'
  })
  window.addEventListener('online', function(e) {
    offlineInfoEl.style.display = 'none'
  })
}

function fetchAndRender(showName) {
  return fetch(`https://api.tvmaze.com/search/shows?q=${showName}`)
    .then(response => response.json())
    .then(json => renderShows(json))
    .then(() => addToSearchedShowList({ showName }))
    .catch(e => console.log('Could not fetch, are you connected?'))
}

function addToSearchedShowList({ showName }) {
  let successList = getShowsFromLocalStorage('success')
  successList = Array.from(new Set(successList).add(showName))
  setShowsToLocalStorage({ listName: 'success', shows: successList })
  renderSearchShowsList()
}

function renderSearchShowsList() {
  const searchShowsList = document.getElementById('searched-shows-list')
  const successList = getShowsFromLocalStorage('success')
  searchShowsList.innerHTML = ''
  successList.forEach(show => searchShowsList.appendChild(createSearchShowListItem({ show })))
}

function getShowsFromLocalStorage(listName) {
  return JSON.parse(localStorage.getItem(listName) || '[]')
}

function setShowsToLocalStorage({ listName, shows }) {
  localStorage.setItem(listName, JSON.stringify(shows))
}

function createSearchShowListItem({ show }) {
  const li = document.createElement('li')
  li.classList.add('searched-shows__list-item')
  const btn = document.createElement('button')
  btn.classList.add('no-button')
  btn.innerText = show

  li.appendChild(btn)
  return li
}

function renderShows(showList) {
  const showResultContainer = document.getElementById('show-results')
  showResultContainer.innerHTML = ''

  showList.forEach(({ show }) => {
    const showContainer = document.createElement('article')
    showContainer.classList.add('show-container')
    const showName = document.createElement('h2')
    const showImage = document.createElement('img')
    const showDescription = document.createElement('section')
    showDescription.classList.add('show-description')
    const showMeta = document.createElement('div')

    showName.innerText = show.name
    showDescription.innerHTML = show.summary
    // Add crossorigin attribute to handle opaque response
    // Why? -> https://cloudfour.com/thinks/when-7-kb-equals-7-mb/
    showImage.setAttribute('crossorigin', 'anonymous')
    showImage.src =
      show.image && show.image.medium
        ? show.image.medium.replace('http:', 'https:')
        : 'Poster404.png'

    showContainer.appendChild(showMeta)
    showContainer.appendChild(showImage)

    showMeta.appendChild(showName)
    showMeta.appendChild(showDescription)

    showResultContainer.appendChild(showContainer)
  })
}

# pwa

Search tv shows with offline support

Run the application by `npx serve`

Tunnel via `ssh.localhost.run` to run on https

Type `ssh -R 80:localhost:5000 ssh.localhost.run`

<img src="./app-demo.png" alt="App Demo" width="300"/>
